<!DOCTYPE html>
<!--[if lte IE 9]>      <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html <?php language_attributes(); ?> > <!--<![endif]-->
<head>
	<title><?php wp_title(''); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=11,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	
	<?php get_template_part( 'template-parts/analyticstracking' ); ?>
	
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">
		
	<?php get_template_part( 'template-parts/navigation' ); ?>
