module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        postcss: {
            options: {
                map: false,
                processors: [
                    require('pixrem')(),
                    require('autoprefixer')({
                        browsers: 'last 2 versions'
                    }),
                    require('cssnano')()
                ],
            },
            dist: {
                expand: true,
                flatten: true,
                src: 'style.css'
            }
        },
        uglify: {
            files: {
                src: 'js/*.js',
                dest: 'js/min/',
                expand: true,
                flatten: true,
                ext: '.min.js'
            }
        },
        jshint: {
            all: ['Gruntfile.js', 'js/*.js'],
            options: {
                expr: true,
                boss: true,
                loopfunc: true,
                sub: true,
                newcap: false,
            }
        },
        notify: {
            sass: {
                options: {
                    title: "CSS Files Compiled",
                    message: "SASS task complete"
                }
            }
        },
        sass: {
            dist: {
                files: {
                    "style.css": "assets/scss/main.scss"
                }
            }
        },
        browserSync: {
            dev: {
                options: {
                    files: ['style.css', 'js/**/*.js', '**/*.php'],
                    watchTask: true,
                    // proxy: "***.dev",
                    proxy: "localhost:80/wp",
                    tunnel: "external",
                    host: '192.168.0.102'
                }
            }
        },
        watch: {
            gruntfile: {
                files: 'Gruntfile.js',
                tasks: ['jshint'],
            },
            markup: {
                files: ['*.php', '*/**/*.php'],
            },
            scripts: {
                files: ['assets/js/*.js'],
                tasks: ['jshint', 'uglify'],
                options: {
                    spawn: true,
                },
            },
            styles: {
                files: ['assets/scss/**'],
                tasks: ['sass', 'notify:sass'],
            },

        }
    });

    grunt.registerTask('production', ['uglify', 'jshint', 'sass', 'postcss']);
    grunt.registerTask('default', ['browserSync', 'watch']);
};
