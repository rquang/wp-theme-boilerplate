<?php

// LOAD STYLESHEET
wp_enqueue_style( 'theme-styles', get_stylesheet_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory() . '/style.css' ) );

// LOAD SCRIPTS
function load_scripts() {
    global $post;

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/min/modernizr.min.js');
	wp_enqueue_script( 'lazysizes', get_template_directory_uri() . '/js/min/lazysizes.min.js');
	
	// wp_enqueue_script( 'main', get_template_directory_uri() . '/js/min/main.min.js', array( 'jquery' ), null);	
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js');	
}
add_action( 'wp_enqueue_scripts', 'load_scripts' );

?>