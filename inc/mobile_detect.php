<?php
require_once 'lib/Mobile_Detect.php';

add_filter( 'body_class','mobile_detect' );
function mobile_detect($classes) {
	$detect = new Mobile_Detect;
	$type;
	if( $detect->isTablet() ){
		$type = 'tablet';
	} elseif ( $detect->isMobile() ) {
		$type = 'mobile';
	} else {
		$type = 'desktop';
	}

	$classes[] = $type;
	
   return $classes;
}

?>